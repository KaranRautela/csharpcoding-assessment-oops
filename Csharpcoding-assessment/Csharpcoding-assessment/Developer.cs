﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharpcoding_assessment
{
    internal class Developer
    {
        public int DeveloperID;
        public string DeveloperName;
        string ProjectAssigned;
        DateTime JoiningDate;
        public static int count = 0;
        public float netSalary;

        virtual public void getDetails()
        {
            Console.WriteLine("Enter your id:");
            DeveloperID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter your name:");
            DeveloperName = Console.ReadLine();
            Console.WriteLine("Enter joining date(dd-mm-yyyy):");
            JoiningDate = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Name of Project assigned:");
            ProjectAssigned = Console.ReadLine();
            count += 1;
        }

        virtual public void displayDetails()
        {
            Console.WriteLine("ID is: " + DeveloperID);
            Console.WriteLine("Name is: " + DeveloperName);
            Console.WriteLine("Date of Joining: " + JoiningDate);
            Console.WriteLine("Project assigned: " + ProjectAssigned);
        }

    }
}
