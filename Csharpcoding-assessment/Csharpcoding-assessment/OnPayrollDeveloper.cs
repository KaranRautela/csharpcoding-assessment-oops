﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharpcoding_assessment
{
    internal class OnPayrollDeveloper:Developer
    {
        string dept;
        string manager;
        float exp;
        float baseSalary;
        float da;
        float hra;
        float pf;

        override public void getDetails()
        {
            base.getDetails();
            Console.WriteLine("Enter department:");
            dept = (Console.ReadLine());
            Console.WriteLine("Enter Manager Name:");
            manager = (Console.ReadLine());
            Console.WriteLine("Enter your Experience:");
            exp = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter your base salary:");
            baseSalary = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter da:");
            da = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter hra:");
            hra = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter pf: ");
            pf = float.Parse(Console.ReadLine());
            calculateSalary();
        }

        override public void displayDetails()
        {
            base.displayDetails();
            Console.WriteLine("Department: " + dept);
            Console.WriteLine("Manager: " + manager);
            Console.WriteLine("Experience: " + exp);
            Console.WriteLine("da: " + da);
            Console.WriteLine("hra: " + hra);
            Console.WriteLine("pf: " + pf);
            Console.WriteLine("NetSalary: " + netSalary);

        }

        float calculateSalary()
        {
            netSalary = baseSalary + da + hra - pf;
            return netSalary;
        }
    }
}
