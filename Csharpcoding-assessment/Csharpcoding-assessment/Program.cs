﻿namespace Csharpcoding_assessment
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Developer> list = new List<Developer>();
            byte ch;
            byte ch2;
            int devID;
            Console.WriteLine("-----MENU-----");
            Console.WriteLine("Enter Choice:\n 1:Create Developer\n2:Display Details");
            ch = Byte.Parse(Console.ReadLine());
            if (ch == 1)
            {
                Console.WriteLine("Type of Developer:\n1:On Contract\n2:On Payroll");
                ch2 = Byte.Parse(Console.ReadLine());
                if (ch2==1) {
                    Developer dev = new OnContracrDeveloper();
                    dev.getDetails();
                    list.Add(dev);
    ;                }
                else if (ch2 == 2)
                {
                    Developer dev = new OnPayrollDeveloper();
                    dev.getDetails();
                    list.Add(dev);

                }
                else
                {
                    Console.WriteLine("Wrong input");
                }
            }
            else if (ch == 2)
            {
                Console.WriteLine("Enter Developer ID:");
                devID = int.Parse(Console.ReadLine());
                var developer = from devp in list where devp.DeveloperID == devID select devp;
                foreach (Developer dev in developer) {
                        dev.displayDetails(); 
                }
            }
            else
            {
                Console.WriteLine("Wrong input");
            }

            //Display all records
            var developers = from dev in list select dev;
            foreach(Developer dev in developers)
            {
                dev.displayDetails();
            }

            //Display records of developer whose netSalary is > 20000
            var netSalary = from dev in list where dev.netSalary > 20000 select dev;
            foreach (Developer dev in netSalary)
            {
                dev.displayDetails();
            }

            //Display records of developer where developer name contains 'D'
            var nameContainsD = from dev in list where dev.DeveloperName.Contains('D') select dev;
            foreach (Developer dev in nameContainsD)
            {
                dev.displayDetails();
            }
        }
    }
}