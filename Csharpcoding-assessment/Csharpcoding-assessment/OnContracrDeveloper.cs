﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharpcoding_assessment
{
    internal class OnContracrDeveloper:Developer
    {
        float duration;
        float charges_per_day;
        

        override public void getDetails()
        {
            base.getDetails();
            Console.WriteLine("Enter duration:");
            duration = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter charges per day:");
            charges_per_day = float.Parse(Console.ReadLine());
            CalculateNetSalary();
        }

        override public void displayDetails()
        {
            base.displayDetails();
            Console.WriteLine("Contract Duration: " + duration);
            Console.WriteLine("Charges:" + charges_per_day);
            Console.WriteLine("Net Salary:" + netSalary);
            ;
        }

        public void CalculateNetSalary() { 
        netSalary = duration * charges_per_day;
        }
    }
}
